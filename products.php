<?php
// Connect to the database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "clothing_store";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Get the request method
$method = $_SERVER['REQUEST_METHOD'];

// Get the id parameter from the query string
$id = isset($_GET['id']) ? $_GET['id'] : null;

// Handle different request methods
switch ($method) {
    case 'GET':
        // If id is not null, get a single product by id
        if ($id) {
            $sql = "SELECT * FROM products WHERE id = $id";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // Output the product data as an html form for editing
                $row = $result->fetch_assoc();
                echo "<h1>Edit Product</h1>";
                echo "<form method='POST' action='products.php?id=$id'>";
                echo "<label for='name'>Name:</label>";
                echo "<input type='text' id='name' name='name' value='" . $row['name'] . "' required><br>";
                echo "<label for='category'>Category:</label>";
                echo "<input type='text' id='category' name='category' value='" . $row['category'] . "' required><br>";
                echo "<label for='price'>Price:</label>";
                echo "<input type='number' id='price' name='price' value='" . $row['price'] . "' required><br>";
                echo "<label for='stock'>Stock:</label>";
                echo "<input type='number' id='stock' name='stock' value='" . $row['stock'] . "' required><br>";
                echo "<input type='submit' value='Update'>";
                echo "</form>";
                echo "<form method='POST' action='products.php?id=$id&_method=DELETE'>";
                echo "<input type='submit' value='Delete'>";
                echo "</form>";
            } else {
                // No product found with that id
                echo "No product found with id = $id";
            }
        } else {
            // If id is null, get all products
            $sql = "SELECT * FROM products";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                // Output all products data as an html table with links for editing and deleting
                echo "<h1>All Products</h1>";
                echo "<table border='1'>";
                echo "<tr><th>Id</th><th>Name</th><th>Category</th><th>Price</th><th>Stock</th><th>Actions</th></tr>";
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['category'] . "</td>";
                    echo "<td>" . $row['price'] . "</td>";
                    echo "<td>" . $row['stock'] . "</td>";
                    echo "<td><a href='products.php?id=" . $row['id'] . "'>Edit</a></td>";
                    echo "</tr>";
                }
                echo "</table>";
            } else {
                // No products found in the database
                echo "No products found";
            }
            // Output a form for creating a new product
            echo "<h1>Create Product</h1>";
            echo "<form method='POST' action='products.php'>";
            echo "<label for='name'>Name:</label>";
            echo "<input type='text' id='name' name='name' required><br>";
            echo "<label for='category'>Category:</label>";
            echo "<input type='text' id='category' name='category' required><br>";
            echo "<label for='price'>Price:</label>";
            echo "<input type='number' id='price' name='price' required><br>";
            echo "<label for='stock'>Stock:</label>";
            echo "<input type='number' id='stock' name='stock' required><br>";
            echo "<input type='submit' value='Create'>";
            echo "</form>";
        }
        break;
    case 'POST':
        // Check if the request has a hidden _method parameter for overriding the method
        if (isset($_POST['_method'])) {
            // If the _method parameter is DELETE, handle it as a DELETE request
            if ($_POST['_method'] == 'DELETE') {
                // Validate the id parameter
                if ($id) {
                    // Delete the product from the database
                    $sql = "DELETE FROM products WHERE id = $id";
                    if ($conn->query($sql) === TRUE) {
                        // Output a success message and a link to go back to the products page
                        echo "Product deleted successfully<br>";
                        echo "<a href='products.php'>Go back to products</a>";
                    } else {
                        // Output an error message
                        echo "Error: " . $sql . "<br>" . $conn->error;
                    }
                } else {
                    // Output a validation error message
                    echo "Please provide a valid id";
                }
            } else {
                // Output an invalid method override message
                echo "Invalid method override";
            }
        } else {
            // If the request has no _method parameter, handle it as a normal POST request
            // Get the product data from the request body
            $name = $_POST['name'];
            $category = $_POST['category'];
            $price = $_POST['price'];
            $stock = $_POST['stock'];

            // Validate the product data
            if ($name && $category && $price && $stock) {
                // Insert the product data into the database
                $sql = "INSERT INTO products (name, category, price, stock) VALUES ('$name', '$category', $price, $stock)";
                if ($conn->query($sql) === TRUE) {
                    // Output a success message and a link to go back to the products page
                    echo "New product created successfully<br>";
                    echo "<a href='products.php'>Go back to products</a>";
                } else {
                    // Output an error message
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            } else {
                // Output a validation error message
                echo "Please provide valid product data";
            }
        }
        break;
    case 'PUT':
        // Get the product data from the request body
        parse_str(file_get_contents("php://input"), $put_vars);
        $name = $put_vars['name'];
        $category = $put_vars['category'];
        $price = $put_vars['price'];
        $stock = $put_vars['stock'];

        // Validate the product data
        if ($id && $name && $category && $price && $stock) {
            // Update the product data in the database
            $sql = "UPDATE products SET name = '$name', category = '$category', price = $price, stock = $stock WHERE id = $id";
            if ($conn->query($sql) === TRUE) {
                // Output a success message and a link to go back to the products page
                echo "Product updated successfully<br>";
                echo "<a href='products.php'>Go back to products</a>";
            } else {
                // Output an error message
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        } else {
            // Output a validation error message
            echo "Please provide valid product data and id";
        }
        break;
    case 'DELETE':
        // Output a method not allowed message
        echo "Method not allowed";
        break;
    default:
        // Output an invalid request method message
        echo "Invalid request method";
        break;
}

// Close the database connection
$conn->close();
